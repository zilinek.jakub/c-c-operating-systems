#include <cstring>
#include <string>


using namespace std;
uint32_t VynasobToPls(uint32_t base, uint32_t exponent){
    uint32_t tmp = base;
    for (int i = 1 ; i != exponent ; i++) {
        tmp *= base;
    }
    return tmp;
}

void IntToChar(uint32_t n , char * mem){
    int i;
    char tmp[33];
    for( i=0; n > 0; i++){
        tmp[i]=char(n%2 + 48);
        n=n/2;
    }
    for (; i != 32 ; i++)
        tmp[i] = '0';
    // todo fill with 0
    tmp[32] = '\0';

    // reversing tmp
    int end = strlen(tmp) - 1;
    int begin;
    for (begin = 0; begin < strlen(tmp); begin++) {
        mem[begin] = tmp[end];
        end--;
    }

    mem[begin] = '\0';
}

int CountZeros(uint32_t number){
    char tmp[33];
    IntToChar(number , tmp);

    int res = 0;
    for (int i = 0 ; i < strlen(tmp); i++)
        if (tmp[i] == '0')
            res++;
    return res;
}

int RemoveFirstZero(uint32_t & number){
    char tmp[33];
    IntToChar(number , tmp);
    int index = -1;
    for (int i = 0 ; i < 32 ; i++){
        if (tmp[i] == '0'){
            tmp[i] = '1';
            index = i;
            break;
        }
    }
    if (index == -1)
        return -1;

    if (index != 31) {
        number = 0;
        for (int i = 0; i < 32; i++) {
            if (tmp[i] == '1')
                number += VynasobToPls(2, 31 - i);
        }
    }
    else
        number = 4294967295;
    return index;
}
