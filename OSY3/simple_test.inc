#define DISK_SECTORS 8192 // 512,1024,4000,8000

#include <iostream>
#include <string>

static FILE  * g_Fp = NULL;

static size_t      diskRead                                ( size_t            sectorNr,
                                                             void            * data,
                                                             size_t            sectorCnt )
{
  if ( g_Fp == NULL
       || sectorNr + sectorCnt > DISK_SECTORS )
    return 0;
  fseek ( g_Fp, sectorNr * SECTOR_SIZE, SEEK_SET );
  return fread ( data, SECTOR_SIZE, sectorCnt, g_Fp );
}
//-------------------------------------------------------------------------------------------------
static size_t      diskWrite                               ( size_t            sectorNr,
                                                             const void      * data,
                                                             size_t            sectorCnt )
{
  if ( g_Fp == NULL
       || sectorNr + sectorCnt > DISK_SECTORS )
    return 0;
  fseek ( g_Fp, sectorNr * SECTOR_SIZE, SEEK_SET );
  return fwrite ( data, SECTOR_SIZE, sectorCnt, g_Fp );
}
//-------------------------------------------------------------------------------------------------
static TBlkDev     createDisk                              ( void )
{
  char       buffer[SECTOR_SIZE];

  memset    ( buffer, 0, sizeof ( buffer ) );
  g_Fp = fopen ( "/tmp/disk_content", "w+b" );
  if ( ! g_Fp )
    throw "Error creating backed block device";

  for ( int i = 0; i < DISK_SECTORS; i ++ )
    if ( fwrite ( buffer, sizeof ( buffer ), 1, g_Fp ) != 1 )
      throw "Error creating backed block device";

  TBlkDev res;
  res . m_Sectors = DISK_SECTORS;
  res . m_Read    = diskRead;
  res . m_Write   = diskWrite;
  return res;
}
//-------------------------------------------------------------------------------------------------
static TBlkDev     openDisk                                ( void )
{
  g_Fp = fopen ( "/tmp/disk_content", "r+b" );
  if ( ! g_Fp )
    throw "Error opening backend block device";
  fseek ( g_Fp, 0, SEEK_END );
  if ( ftell ( g_Fp ) != DISK_SECTORS * SECTOR_SIZE )
  {
    fclose ( g_Fp );
    g_Fp = NULL;
    throw "Error opening backend block device";
  }

  TBlkDev  res;
  res . m_Sectors = DISK_SECTORS;
  res . m_Read    = diskRead;
  res . m_Write   = diskWrite;
  return res;
}
//-------------------------------------------------------------------------------------------------
static void        doneDisk                                ( void )
{
  if ( g_Fp )
  {
    fclose ( g_Fp );
    g_Fp  = NULL;
  }
}
//-------------------------------------------------------------------------------------------------
static void        testMkFs                                ( void )
{
    assert ( CFileSystem::CreateFs ( createDisk () ) );
    doneDisk ();
}
//-------------------------------------------------------------------------------------------------
static void        testWrite                               ( void )
{
    char      buffer [100];
    char      buffer2 [100] = {0};
    TFile     info;
    CFileSystem * fs = CFileSystem::Mount ( openDisk () );
    assert ( fs );
    fs -> OpenFile ( "hello3", true );
    fs -> CloseFile(0);
    fs -> OpenFile ( "hello2", true );
    fs -> CloseFile(1);
    int fd = fs -> OpenFile ( "hello", true );
    for ( int i = 0; i < 100; i ++ )
        buffer[i] = i;
    assert ( fs -> WriteFile ( fd, buffer, 100 ) == 100 );
    assert ( fs -> CloseFile ( fd ) );

    fd = fs -> OpenFile ( "hello", false );
    assert(fs -> ReadFile (fd , buffer2 , 100 ) == 100);
    for(int i = 0 ;i < 100 ; i++)
        assert(buffer[i] == buffer2[i]);
    assert(fs->CloseFile(fd));
    assert ( fs -> Umount () );
    delete fs;
    doneDisk ();
    fs = fs -> Mount ( openDisk () );
    assert ( fs );

    for ( bool found = fs -> FindFirst ( info ); found; found = fs -> FindNext ( info )   )
        printf ( "%-30s %6zd\n", info . m_FileName, info . m_FileSize );
    fs ->OpenFile("hello3", false);

    assert (fs ->DeleteFile("hello3"));
    assert (fs ->DeleteFile("hello"));
    assert (fs ->DeleteFile("hello2"));
    assert ( ! fs ->DeleteFile("hello2") );


    for ( bool found = fs -> FindFirst ( info ); found; found = fs -> FindNext ( info )   )
        printf ( "%-30s %6zd\n", info . m_FileName, info . m_FileSize );

    assert ( fs -> Umount () );
    delete fs;
    doneDisk ();
}
static void        myTestRead                               ( void )
{
    int testSize = 1000;
    char      buffer [testSize];
    char      buffer2 [testSize];
    TFile     info;
    CFileSystem * fs = CFileSystem::Mount ( openDisk () );
    assert ( fs );


    int fd = fs -> OpenFile ( "hello", true );
    for ( int i = 0; i < testSize; i ++ )
        buffer[i] = i;
    assert ( fs -> WriteFile ( fd, buffer, testSize ) == testSize );
    assert ( fs -> CloseFile ( fd ) );

    fd = fs -> OpenFile ( "hello", false );
    assert(fs ->ReadFile(fd,buffer2, 50) == 50);
//    for (int i = 0 ; i < 50 ; i++)
//        printf("%d ", buffer2[i]);

    assert(fs ->ReadFile(fd,buffer2, 100) == 100);

//    for (int i = 0 ; i < 100 ; i++)
//        printf("%d ", buffer2[i]);

    assert(fs ->ReadFile(fd,buffer2, 1000) == 850);
//    for (int i = 0 ; i < 850 ; i++)
//        printf("%d ", buffer2[i]);



    cout << endl << endl << endl << "------------------------------------------TEST2------------------------------------------" << endl << endl;
    int fd2 = fs -> OpenFile ( "hello2", true );
    for ( int i = 0; i < testSize; i ++ )
        buffer[i] = i;

    assert ( fs -> WriteFile ( fd2, buffer, testSize ) == testSize );
    assert ( fs -> CloseFile ( fd2 ) );
    fd2 = fs -> OpenFile ( "hello2", false );
    for (int i = 0; i < 1000; i++) {
        assert(fs->ReadFile(fd2, buffer2, 1) == 1);
//        printf("%d ", buffer2[0]);
    }





    assert ( fs -> CloseFile ( fd ) );
    assert ( fs -> Umount () );
    delete fs;
    doneDisk ();
    fs = fs -> Mount ( openDisk () );
    assert ( fs );

    for ( bool found = fs -> FindFirst ( info ); found; found = fs -> FindNext ( info )   )
        printf ( "%-30s %6zd\n", info . m_FileName, info . m_FileSize );

    assert ( fs -> Umount () );
    delete fs;
    doneDisk ();
}
static void        myTestOpen                               ( void )
{
    TFile info;
    int testSize = 500;
    char      buffer [testSize];
    CFileSystem * fs = CFileSystem::Mount ( openDisk () );
    assert ( fs );
    assert ( fs -> OpenFile ( "", false ) == -1);
    assert ( fs -> OpenFile ( "hello1", true ) == 0);
    assert ( fs -> OpenFile ( "hello2", true ) == 1);
    assert ( fs -> OpenFile ( "hello3", true ) == 2);
    assert ( fs -> OpenFile ( "hello4", true ) == 3);
    assert ( fs -> OpenFile ( "hello5", true ) == 4);
    assert ( fs -> OpenFile ( "hello5", true ) == 4);
    assert ( fs -> OpenFile ( "hello6", true ) == 5);
    assert ( fs -> OpenFile ( "hello7", true ) == 6);
    assert ( fs -> OpenFile ( "hello8", true ) == 7);
    assert ( fs -> OpenFile ( "hello8", true ) == 7);
    assert ( fs -> OpenFile ( "hello8", true ) == 7);
    assert ( fs -> OpenFile ( "hello8", false ) == 7);
    assert (fs ->CloseFile(2) == true);
    assert ( fs -> OpenFile ( "hello9", true ) == 2);
    assert ( fs -> OpenFile ( "hello3", false ) == -1);
    for ( int i = 0; i < testSize; i ++ )
        buffer[i] = i;
    assert ( fs -> WriteFile ( 1, buffer, testSize ) == testSize );
    assert ( fs->FileSize("hello2") == testSize);
    assert ( fs->FileSize("hello1") == 0 );

    for ( bool found = fs -> FindFirst ( info ); found; found = fs -> FindNext ( info )   )
        printf("%-30s %6zd\n", info.m_FileName, info.m_FileSize);
    cout << "==============" << endl << endl;
    for ( bool found = fs -> FindFirst ( info ); found; found = fs -> FindNext ( info )   ) {
        if (strcmp( info.m_FileName, "hello3") == 0)
            break;
        printf("%-30s %6zd\n", info.m_FileName, info.m_FileSize);
    }
    cout << "==============" << endl << endl;

    for ( bool found = fs -> FindFirst ( info ); found; found = fs -> FindNext ( info )   )
        printf ( "%-30s %6zd\n", info . m_FileName, info . m_FileSize );
    cout << "==============" << endl << endl;


    assert ( fs -> Umount () );
    delete fs;
    doneDisk ();
}
static void        myTestWrite                               ( void )
{

    int testSize = floor(DISK_SECTORS*0.9*512);
    char      buffer [testSize];
    char      buffer2 [testSize];
    CFileSystem * fs = CFileSystem::Mount ( openDisk () );
    assert ( fs );


    int fd = fs -> OpenFile ( "hello", true );
    for ( int i = 0; i < testSize; i ++ )
        buffer[i] = i;
    assert ( fs -> WriteFile ( fd, buffer, testSize ) == testSize );
    assert ( fs -> CloseFile ( fd ) );

    fd = fs -> OpenFile ( "hello", false );
    assert(fs ->ReadFile(fd,buffer2, testSize) == testSize);
//    for (int i = 0 ; i < testSize ; i++)
//        printf("%d ", buffer2[i]);
    assert ( fs -> CloseFile ( fd ) );
    assert ( fs -> Umount () );
    delete fs;
    doneDisk ();
}
static void        myTestWrite2                               ( void )
{
    // 8192, 128
    int testSize = 4097;
    char      buffer [testSize];
    char      bufferTest [testSize];
    CFileSystem * fs = CFileSystem::Mount ( openDisk () );
    assert ( fs );

    for (int i = 0; i < 129; i++) {
        if (i == 128) {
            assert(fs->OpenFile(to_string(i).c_str(), true) == -1);
            continue;
        }

        int fd = fs->OpenFile( to_string(i).c_str() , true);
        for (int i = 0; i < testSize; i++)
            buffer[i] = i;
        assert (fs->WriteFile(fd, buffer, testSize) == testSize);
        assert (fs->FileSize(to_string(i).c_str()) == testSize);
        assert (fs->CloseFile(fd));

        assert(fs->FileSize(to_string(i).c_str()) == testSize);
        fd = fs->OpenFile(to_string(i).c_str(), false);
        assert (fs->ReadFile(fd, bufferTest, testSize) == testSize);
        for (int i = 0 ;i < testSize ; i++)
            assert(buffer[i] == bufferTest[i]);
        assert (fs->CloseFile(fd));
    }


    for (int i = 0; i < 128; i++){
        int fd = fs->OpenFile(to_string(i).c_str(), false);
        assert (fs->ReadFile(fd, bufferTest, testSize) == testSize);
        assert(fs->FileSize(to_string(i).c_str()) == testSize);
        for (int i = 0 ;i < testSize ; i++)
            assert(buffer[i] == bufferTest[i]);
        assert (fs->CloseFile(fd));
    }


    assert ( fs -> Umount () );
    delete fs;
    doneDisk ();
}
static void        myTestDelete                                (void){
    int testSize = 32256;

    // 127*63 -> zaplnit 127 souboru a celkem 8001 sektoru

    // posledni soubor musi mit 58880

    char      buffer [testSize];
    for ( int i = 0; i < testSize; i ++ )
        buffer[i] = i;
    CFileSystem * fs = CFileSystem::Mount ( openDisk () );
    assert ( fs );
    for(int i = 0; i < DIR_ENTRIES_MAX; i++){
        int tmp;
        int testSizeass = testSize;
        std::string s = std::to_string(i);
        assert (fs->OpenFile(s.c_str(), true) == 0);
        if (i == 127){
            char buffer2[58880];
            for (char & j : buffer2)
                j = 0;
            tmp = fs->WriteFile(0, buffer2, 58880);
            testSizeass = 58880;
        }
        else {
            tmp = fs->WriteFile(0, buffer, testSize);
            testSizeass = testSize;
        }
        assert ( tmp == testSizeass );

        assert ( fs -> FileSize( s.c_str() ) == testSizeass );
        assert (fs ->CloseFile(0) == true);
    }
    TFile info{};
    for (int i = 0 ; i < DIR_ENTRIES_MAX; i++) {
        int tmp = fs->OpenFile(to_string(i).c_str(), true);
        fs->CloseFile(tmp);
    }
    for(int i = 0; i < DIR_ENTRIES_MAX; i++){
        std::string s = std::to_string(i);
        assert (fs ->DeleteFile(s.c_str()) == true);
    }
    for ( bool found = fs -> FindFirst ( info ); found; found = fs -> FindNext ( info )   )
        printf ( "%-30s %6zd\n", info . m_FileName, info . m_FileSize );
    assert ( fs -> Umount () );
    delete fs;
    doneDisk ();
}

static void FirstProgtestTest(){
    CFileSystem * fs = CFileSystem::Mount ( openDisk () );
    assert ( fs );

    int testSize = 1024 * 4; // zabere 4096B -> 8 sektoru
    uint8_t buffer[testSize];
    for (int i = 0 ; i < testSize; i++)
        buffer[i] = abs(i);

    for (int i = 0 ; i < 128 ; i++){
        assert ( fs -> OpenFile(to_string(i).c_str(), true) == 0);
        assert ( fs -> WriteFile(0, buffer, testSize) == testSize);
        assert ( fs -> FileSize(to_string(i).c_str()) == testSize);
        assert ( fs -> CloseFile(0) == true);
    }

    for (int i = 0 ; i < 128 ; i++) {
        assert ( fs -> OpenFile(to_string(i).c_str(), false) == 0);
        uint8_t buffer2[4096];
        for (int i = 0 ; i < 4096; i++)
            buffer2[i] = 0;
        assert(fs ->ReadFile(0,buffer2, testSize) == testSize);
//        assert(fs -> FileSize(to_string(i).c_str()) == testSize);
        for (int j = 0; j < testSize ; j++) {
            assert(buffer[j] == buffer2[j]);
        }
        assert ( fs -> CloseFile(0) == true);
    }

    for (int i = 0 ; i < 128 ; i++) {
        if (i % 5 == 0) {
            assert (fs->OpenFile(to_string(i).c_str(), true) == 0);
            assert (fs->CloseFile(0) == true);
        }
    }


    for (int i = 0; i < 128 ; i++) {
        assert(fs ->DeleteFile(to_string(i).c_str()) == true);
    }
//    fs->TestFuction();
    fs->Umount();
    delete fs;
    fs = nullptr;
    fs = CFileSystem::Mount(openDisk());

    cout << "\n\n\n\t############################################### FIRST TEST PASSED ###############################################" << endl << endl << endl;

    for (int i = 0 ; i < 128 ; i++){
//        cout << i * 31 << endl;
        string tmp = to_string(i);
        assert ( fs -> OpenFile(tmp.c_str(), true) == 0);

        uint8_t * bufferU = (uint8_t *)malloc(sizeof(uint8_t ) * i*31);

        for (int j = 0 ; j < i*31 ; j++)
            bufferU[j] = abs(j)%255;
        assert( fs ->WriteFile(0, bufferU, i*31 ) == i*31);
        assert ( fs -> CloseFile(0) == true);

        assert ( fs -> OpenFile(to_string(i).c_str(), false) == 0);
        uint8_t * bufferU2 = (uint8_t *)malloc(sizeof(uint8_t ) * i*31);
        assert (fs ->ReadFile(0,bufferU2,i*31) == i*31);
        for (int j = 0 ; j < i*31 ; j++){
            assert(bufferU[j] == bufferU2[j]);
        }
        assert ( fs -> CloseFile(0) == true);
        free(bufferU);
        free(bufferU2);
    }

    for (int i = 0 ; i < 128 ; i++) {
        assert ( fs -> OpenFile(to_string(i).c_str(), true) == 0);
        assert ( fs -> CloseFile(0) == true);
    }

    for (int i = 0; i < 128 ; i++) {
        assert(fs ->DeleteFile(to_string(i).c_str()) == true);
    }

    fs->Umount();
    delete fs;
}


//-------------------------------------------------------------------------------------------------
int main ( void )
{
    testMkFs();
    FirstProgtestTest();



//    testMkFs ();
//    cout << "=================================myTestRead=================================" << endl << endl << endl;
//    myTestDelete();
//
//    testMkFs ();
//    cout << "=================================testWrite=================================" << endl << endl << endl;
//    testWrite ();
//    testMkFs ();
//    cout << endl << "=================================myTestOpen=================================" << endl << endl << endl;
//    myTestOpen();
//    testMkFs ();
//    cout << "=================================myTestWrite=================================" << endl << endl << endl;
//    myTestWrite();
//    testMkFs ();
//    cout << "=================================myTestWrite2=================================" << endl << endl << endl;
//    myTestWrite2();
     /**/
  return 0;
}
