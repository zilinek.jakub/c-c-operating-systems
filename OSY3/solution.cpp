#ifndef __PROGTEST__
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cstdint>
#include <cassert>
#include <functional>
#include <cmath>
#include <iostream>
#include <fstream>

using namespace std;


/* Filesystem m_size: min 8MiB, max 1GiB
 * Filename length: min 1B, max 28B
 * Sector m_size: 512B
 * Max open files: 8 at a time
 * At most one filesystem mounted at a time.
 * Max file m_size: < 1GiB
 * Max files in the filesystem: 128
 */



/** UINTMAX kdyz je to prazdne, uplne, nemame jmeno...
 *  KDYZ mame jmeno souboru ale ten je prazdny aneb nema sektor, tak je to UINTMAX -1 !
 * */
#define FILENAME_LEN_MAX    28
#define DIR_ENTRIES_MAX     128
#define OPEN_FILES_MAX      8
#define SECTOR_SIZE         512
#define DEVICE_SIZE_MAX     ( 1024 * 1024 * 1024 )
#define DEVICE_SIZE_MIN     ( 8 * 1024 * 1024 )

struct TFile
{
    char            m_FileName[FILENAME_LEN_MAX + 1];
    size_t          m_FileSize;
};

struct TBlkDev
{
    size_t           m_Sectors;
    function<size_t(size_t, void *, size_t )> m_Read;
    function<size_t(size_t, const void *, size_t )> m_Write;
};

#endif /* __PROGTEST__ */
#define myEOF                 (-2)
#define MY_FREE                (-1)
struct CDir{
    CDir() = default;
    char m_name[FILENAME_LEN_MAX + 1];
    uint32_t m_addr;
    uint32_t m_size;
};
struct CFile
{
    TFile file{};
    bool write = false;
    bool open = false;
    uint32_t startBlock = UINT32_MAX;
    int numBlock = 0;
    CFile * before = nullptr;
    CFile * next = nullptr;
    char buffer [SECTOR_SIZE]{};
    uint32_t lastSector = UINT32_MAX;
    uint32_t bufferIndex = 0;   // jak moc plny je buffer
    uint32_t bufferRead = 0;     // pocet prectenych indexu
};

uint32_t toBinLine(size_t num){
    int exp = 31;
    uint32_t res = 0;
    for(size_t i = 0; i < num; i++){
        res = res + pow(2, exp);
        exp --;
    }
    return res;
}
void intToChar(uint32_t n , char * mem){
    int i;
    char tmp[33];
    for( i=0; n > 0; i++){
        tmp[i]=char(n%2 + 48);
        n=n/2;
    }
    for (; i != 32 ; i++)
        tmp[i] = '0';
    tmp[32] = '\0';

    // reversing tmp
    int end = strlen(tmp) - 1;
    size_t begin;
    for (begin = 0; begin < strlen(tmp); begin++) {
        mem[begin] = tmp[end];
        end--;
    }

    mem[begin] = '\0';
}
int countZeros(uint32_t num){
    char test[33];
    int toRet = 0;
    intToChar(num,test);
    for (int i = 0 ; i < 32 ; i++)
        if ( test[i] == '0' )
            toRet ++;
    return toRet;
}

class CFileSystem
{
public:
    bool FatBitMapCompare(){
        for (int i = 0 ; i < 8192; i++){
            if (m_fat[i] != MY_FREE){
                char test[33];
                uint row = i / 32;
                uint column = i % 32;
                intToChar(m_bit[row],test);
                if (test[column] != '1'){
                    cout << "CHYBA -> " << i << " -> " << m_bit[row] << endl;
                    cout << "row: " << row << "column: " << column << " -> " << m_bit[row] << endl;
                    return false;
                }
            }
        }
        return true;
    }
    void TestFuction(){
        FatBitMapCompare();

        cout << "=============================================================================================================" << endl;
        cout << "=====================================================FAT=====================================================" << endl;
        cout << "=============================================================================================================" << endl << endl << endl << endl;

        for (int i = 0 ; i < createDev.m_Sectors ; i++){
            if (m_fat[i] != MY_FREE)
                cout << i << " -> " << m_fat[i] << endl;
        }
        cout << "=============================================================================================================" << endl;
        cout << "=====================================================BIT=====================================================" << endl;
        cout << "=============================================================================================================" << endl << endl << endl << endl;

        for (int i = 0 ; i < createDev.m_Sectors/32 ; i++){
            if (m_bit[i] != 0){
                char test[33];
                cout << i << " - ";
                intToChar(m_bit[i], test);
                for (int j = 0 ; j < 32 ; j++)
                    printf("%c", test[j]);
                cout << endl;
            }
        }
        cout <<  "=====================================================DIR=====================================================" << endl;
        cout << "=============================================================================================================" << endl << endl << endl << endl;

        for (int i = 0 ; i < DIR_ENTRIES_MAX ; i++){
            cout << m_dir[i].m_name << " - " << m_dir[i].m_size << " - " << m_dir[i].m_addr << endl;
        }
    }
    void PrintDirWithIterator(){
        cout << "\n\t ### DIR ###\n\n";
        TFile info;
        for ( bool found = FindFirst ( info ); found; found = FindNext ( info )   )
            printf ( "%-30s %6zd\n", info . m_FileName, info . m_FileSize );

        cout << "\n\t --- DIR PRINT DONE ---\n\n";
    }



    static bool CreateFs ( const TBlkDev   & dev ){
        size_t sectors = 0;
        sectors = dev.m_Sectors;
        int32_t * buffer = new int32_t[sectors];
        uint32_t * bitmap = new uint32_t[sectors/32];
        CDir addDir[DIR_ENTRIES_MAX];

        //vyplnime FAT jao free
        for(size_t i = 0; i < sectors; i ++){
            buffer[i] = MY_FREE;
        }
        size_t fullSectors = ceil((sectors * sizeof(int32_t)) / SECTOR_SIZE); // pocet sektoru pro FAT
        dev.m_Write(0, buffer, fullSectors); //zapsani FATu
        size_t end = fullSectors;
        fullSectors += ceil(sectors/(32 * SECTOR_SIZE/4 )); // za bitmapu
        fullSectors += ceil((DIR_ENTRIES_MAX * sizeof (CDir))/SECTOR_SIZE);// za addDir

        //zapiseme 1 za kazdy obsazeny sektor
        for(uint32_t i = 0; i < fullSectors/32; i++ ){
            bitmap[i] = UINT32_MAX;
        }
        uint32_t index = fullSectors/32;
        size_t resSectors = fullSectors % 32;
        if(resSectors != 0){
            bitmap[index] = toBinLine(resSectors);
            index ++;
        }
        //zapiseme 0 na dalsi sektory
        for(uint32_t i = index; i < sectors/32; i++){
            bitmap[i] = 0;
        }

        size_t max = ceil(sectors/(32 * SECTOR_SIZE/4 )) ;
        dev.m_Write( end , bitmap, max);

        // zapis prazdnych policek na soubory -> m_addr = UINT32_MAX
        for(int i = 0; i < DIR_ENTRIES_MAX; i++){
            CDir tmp = CDir();
            for(int j = 0; j < FILENAME_LEN_MAX + 1; j++){
                tmp.m_name[j] = '\0';
            }
            tmp.m_addr = UINT32_MAX;
            tmp.m_size = 0;
            addDir[i] = tmp;
        }
        dev.m_Write( end + max , addDir,  (DIR_ENTRIES_MAX * sizeof (CDir))/SECTOR_SIZE );
        delete[] buffer;
        delete[] bitmap;
        return true;

    }                               /// OK
    static CFileSystem * Mount ( const TBlkDev   & dev ){
        CFileSystem * system = new CFileSystem; //todo kde udelam free?
        //nastavime promene
        system->createDev = dev;
        system->numFiles = 0;
        system->ourFiles = nullptr;
        system->iterator = nullptr;
        system->lastFile = nullptr;
        //precteme si BITMAPU

        system->fatSize = (dev.m_Sectors * sizeof (uint32_t)) / SECTOR_SIZE;
        system->bitSize = dev.m_Sectors/(32 * SECTOR_SIZE/4 );
        system->dirSize = (DIR_ENTRIES_MAX * sizeof (CDir))/SECTOR_SIZE;

        system->m_bit = new uint32_t [dev.m_Sectors/32];
        system->m_dir = new CDir[DIR_ENTRIES_MAX];
        system->m_fat = new int32_t [dev.m_Sectors];

        dev.m_Read(0, system->m_fat, system->fatSize);
        dev.m_Read(system->fatSize, system->m_bit, system->bitSize );
        dev.m_Read( system->fatSize + system->bitSize , system->m_dir, system->dirSize );

        for(uint32_t i = 0; i < DIR_ENTRIES_MAX; i++) {
            if (system->m_dir[i].m_addr != UINT32_MAX) { //
                if (system->m_dir[i].m_addr == UINT32_MAX - 1)
                    system->makeNewFile(system->m_dir[i].m_name, UINT32_MAX, system->m_dir[i].m_size);
                else
                    system->makeNewFile(system->m_dir[i].m_name, system->m_dir[i].m_addr, system->m_dir[i].m_size);
            }
        }
        system->numOpenFiles = 0;
        for(auto & i : system->fileDetec){
            i = nullptr;
        }
        return system;
    }                         /// OK
    bool Umount ( void ) {
        for (int i = 0; i < OPEN_FILES_MAX; i++) {
            if (fileDetec[i] != nullptr) {
                CloseFile(i);
            }
        }
        CFile * curr;
        CFile * tmp;
        curr = ourFiles;
        for (uint32_t i = 0; i < numFiles; i++){
            tmp = curr;
            curr = tmp->next;
            delete tmp;
        }

        createDev.m_Write(0, m_fat, fatSize);
        createDev.m_Write(fatSize, m_bit, bitSize );
        createDev.m_Write(fatSize + bitSize , m_dir, dirSize );

        delete []m_bit;
        delete []m_dir;
        delete []m_fat;
        return true;
    }                                                        /// OK
    size_t FileSize ( const char      * fileName ){
        bool founded;
        CFile * nextFile = returnFile(fileName, founded);
        if(!founded) return -1;
        return nextFile->file.m_FileSize;
    }                               /// OK
    int OpenFile ( const char * fileName, bool writeMode ){
        bool founded;
        CFile * nextFile = returnFile(fileName, founded);
        if(!founded && !writeMode) return -1;
        if(founded){
            if(nextFile->open) {
                for (int i = 0 ; i < OPEN_FILES_MAX ; i++){
                    if (fileDetec[i] == nextFile) {
                        CloseFile(i);
                        break;
                    }
                }
            }
            if(numOpenFiles == OPEN_FILES_MAX) return -1;
            numOpenFiles++;
            nextFile->open = true;
            nextFile->write = writeMode;
            nextFile->bufferIndex = 0;
            nextFile->bufferRead = 0;
            nextFile->lastSector = UINT32_MAX;
            if(writeMode){
                nextFile->file.m_FileSize = 0;
                nextFile->numBlock = 0;
                uint32_t size = 0;
                if (nextFile->startBlock != UINT32_MAX){
                    uint32_t * removed = removeFromFat(nextFile->startBlock, size);
                    removeFromBitmap(removed, size);
                    free(removed);
                }
                setSizeDir(fileName, 0);
                setAddresDir(fileName, UINT32_MAX - 1); // TODO -1
                nextFile->startBlock = UINT32_MAX;
            }
            return findNextIndex(nextFile);
        }
        if(numOpenFiles == OPEN_FILES_MAX) return -1;
        if(numFiles == DIR_ENTRIES_MAX) return -1;
        CFile * newFile = new CFile;
        memcpy(newFile->file.m_FileName,fileName, strlen(fileName) <= FILENAME_LEN_MAX ? strlen(fileName) : FILENAME_LEN_MAX);
        newFile->file.m_FileName[strlen(fileName) <= FILENAME_LEN_MAX ? strlen(fileName) : FILENAME_LEN_MAX] = '\0';
        newFile->file.m_FileSize = 0;
        newFile->numBlock = 0;
        newFile->bufferIndex = 0;
        newFile->bufferRead = 0;
        newFile->open = true;
        newFile->write = writeMode;
        newFile->next = nullptr;
        newFile->before = lastFile;
        if(lastFile != nullptr)
            lastFile->next = newFile;
        if(lastFile == nullptr) {
            ourFiles = newFile;
            iterator = newFile;
        }
        lastFile = newFile;
        numFiles++;
        numOpenFiles++;
        newFile->startBlock = UINT32_MAX;
        writeToDir(newFile->file.m_FileName, newFile->startBlock - 1);
        //setSizeDir(newFile->file.m_FileName, 0);
        return findNextIndex(newFile);
    }                       ///
    size_t ReadFile ( int fd, void * data, size_t len ){
        if(fd < 0 || fd >= OPEN_FILES_MAX) return 0;
        CFile * myFile = fileDetec[fd];
        if(!myFile) return 0;
        if (myFile->file.m_FileSize == 0) return 0;
        size_t canBeRead = myFile->bufferIndex - myFile->bufferRead;
        char * data2 = (char*)data;
        size_t index = 0;
        if(len <= canBeRead){
            memcpy(&data2[index], &myFile->buffer[myFile->bufferRead], len);
            myFile->bufferRead += len;
            data = data2;
            return len;
        }

        memcpy(&data2[index], &myFile->buffer[myFile->bufferRead], canBeRead);
        myFile->bufferIndex = 0;
        myFile->bufferRead = 0;
        index += canBeRead;

        uint size = myFile->startBlock;
        if(size == UINT32_MAX){
            data = data2;
            return 0;
        }
        uint32_t * myFat = readFromFat(size, myFile->lastSector); // TODO ???


        for(uint32_t i = 0; i < size; i++ ){
            createDev.m_Read(myFat[i],myFile->buffer,1);
            myFile->bufferIndex = SECTOR_SIZE;
            if(i == size-1 && myFile->file.m_FileSize % SECTOR_SIZE != 0){
                myFile->bufferIndex = myFile->file.m_FileSize % SECTOR_SIZE;
            }
            myFile->bufferRead = 0;
            if(index + SECTOR_SIZE > len){
                unsigned long toWrite = myFile->bufferIndex < (len - index) ?  myFile->bufferIndex : (unsigned long)(len - index) ;
                memcpy(&data2[index], &myFile->buffer[myFile->bufferRead], toWrite);
                myFile->bufferRead = toWrite;
                myFile->lastSector = myFat[i];
                index += toWrite;
                break;
            }
            else{
                memcpy(&data2[index], &myFile->buffer[myFile->bufferRead], myFile->bufferIndex);
                index += myFile->bufferIndex;
                myFile->bufferRead = myFile->bufferIndex;
                myFile->lastSector = myFat[i]; // TODO ???
            }
        }
        data = data2;
        free(myFat);
        return index;


    }                          ///
    size_t WriteFile ( int fd, const void * data, size_t len ){
        if(fd < 0 || fd >= OPEN_FILES_MAX) return 0;
        CFile * myFile = fileDetec[fd];
        if(!myFile) return 0;
        int numInBuffer = myFile->bufferIndex;
        size_t freeInBuffer = SECTOR_SIZE - numInBuffer;
        // alokace prvniho sektoru, pridani do FAT a bitmapy
        if (myFile->startBlock == UINT32_MAX){
            int32_t sector = findFreeSector();
            if (sector == -1)
                return 0;
            myFile->startBlock = sector;
            myFile->lastSector = sector;
            myFile->bufferIndex = 0;
            myFile->numBlock = 1;
            writeToFat(sector, UINT32_MAX);
            setAddresDir(myFile->file.m_FileName, sector);
        }


        char * data2 = (char *)data;
        int startLen = len;
        if(freeInBuffer > 0){
            if (len < freeInBuffer) {
                memcpy(&myFile->buffer[myFile->bufferIndex], data, len);
                uint32_t sector = findLastSector(myFile->startBlock);
                myFile->file.m_FileSize += len;
                setSizeDir(myFile->file.m_FileName, myFile->file.m_FileSize );
                createDev.m_Write(sector, myFile->buffer, 1);
                myFile->bufferIndex += len;
                return len;
            }
            else {
                memcpy( &myFile->buffer[myFile->bufferIndex], &data2[0], freeInBuffer);
                uint32_t sector = findLastSector(myFile->startBlock);
                myFile->file.m_FileSize += freeInBuffer;
                setSizeDir(myFile->file.m_FileName, myFile->file.m_FileSize );
                createDev.m_Write(sector, myFile->buffer, 1);
                len -= freeInBuffer;
                myFile->bufferIndex = SECTOR_SIZE;
                myFile->bufferRead = 0;
            }
        }
        int index = freeInBuffer;
        while (len > 0){
            // nahravame cely sektor do noveho sektoru
            if (len >= SECTOR_SIZE){
                int sector = findFreeSector();
                if (sector == -1 ) {
                    myFile->bufferIndex = SECTOR_SIZE;
                    freeInBuffer = 0;
                    break;
                }
                writeToFat(sector,myFile->startBlock);
                createDev.m_Write(sector, &data2[index], 1);
                len -= SECTOR_SIZE;
                myFile->lastSector = sector;
                myFile->file.m_FileSize += SECTOR_SIZE;
                myFile->numBlock += 1;
                index += SECTOR_SIZE;
            }
                // Ukoncujeme cyklus, len bude 0, nahravame do posledni
                //  sektoru ktery bude plny jen z casti
            else{
                int sector = findFreeSector();
                if (sector == -1 )
                    break;
                writeToFat(sector,myFile->startBlock);
                memcpy( &myFile->buffer[0], &data2[index], len);
                createDev.m_Write(sector, myFile->buffer, 1);
                myFile->bufferIndex = len;
                myFile->lastSector = sector;
                myFile->file.m_FileSize += len;
                myFile->numBlock += 1;
                len = 0;
            }
        }
        setSizeDir(myFile->file.m_FileName, myFile->file.m_FileSize );
        return startLen - len;
    }                   ///
    bool CloseFile ( int fd ){
        if(fd < 0 || fd >= OPEN_FILES_MAX) return false;
        CFile * myFile = fileDetec[fd];
        if(!myFile) return false;
        myFile->open = false;
        myFile->lastSector = UINT32_MAX;
        myFile->bufferIndex = 0;
        fileDetec[fd] = nullptr;
        numOpenFiles --;
        return true;
    }                                                    /// OK
    bool DeleteFile ( const char * fileName ){
        bool founded;
        CFile * nextFile = returnFile(fileName, founded);
        if(!founded) return false;
        if (nextFile->open){
            for (int i = 0 ; i < OPEN_FILES_MAX ; i++) {
                if (fileDetec[i] && strcmp(fileDetec[i]->file.m_FileName, fileName) == 0){
                    CloseFile(i);
                    break;
                }
            }
        }
        if(nextFile->before != nullptr)
            nextFile->before->next = nextFile->next;
        if(nextFile->next != nullptr)
            nextFile->next->before = nextFile->before;
        if(iterator == nextFile){
            iterator = nextFile->next;
        }
        if(lastFile == nextFile){
            lastFile = nextFile->before;
        }
        uint32_t size;
        if (nextFile->startBlock != UINT32_MAX){
            uint32_t *toRemove = removeFromFat(nextFile->startBlock, size);//todo
            removeFromBitmap(toRemove, size);
            free(toRemove);
        }
        removeFromDir(nextFile->file.m_FileName);
        if (nextFile == ourFiles){
            ourFiles = nextFile->next;
        }
        delete nextFile;
        numFiles --;
        return true;
    }                                    /// OK
    bool FindFirst  ( TFile & file ){
        if(numFiles == 0){
            return false;
        }
        iterator = ourFiles->next;
        memcpy(file.m_FileName, ourFiles->file.m_FileName, strlen(ourFiles->file.m_FileName));
        file.m_FileName[strlen(ourFiles->file.m_FileName)] = '\0';
        file.m_FileSize = ourFiles->file.m_FileSize;
        return true;
    }                                             /// OK
    bool FindNext ( TFile & file ){
        if( iterator == nullptr || numFiles == 0){
            return false;
        }
        memcpy(file.m_FileName, iterator->file.m_FileName, strlen(iterator->file.m_FileName));
        file.m_FileName[strlen(iterator->file.m_FileName)] = '\0';
        file.m_FileSize = iterator->file.m_FileSize;
        iterator = iterator->next;
        return true;
    }                                               /// OK
    int FreeSectorsCount() const{
        long int toReturn = 0;
        for (int i = 0 ; i < 256; i++){
            toReturn += countZeros(m_bit[i]);
        }
        return toReturn;
    }                                                 /// OK
private:
    TBlkDev createDev; //todo potrebovala bych to jako pointer
    CFile * ourFiles;
    CFile * iterator;
    CFile * lastFile;
    uint32_t numFiles;
    uint32_t numOpenFiles;
    CFile * fileDetec[OPEN_FILES_MAX];

    int32_t *  m_fat;
    uint32_t   fatSize;

    uint32_t * m_bit;
    uint32_t   bitSize;

    CDir *     m_dir;
    uint32_t   dirSize;


    void makeNewFile(char * name, uint32_t addr, uint32_t size){
        //vytvorime novy soubor volame pri mount
        //soubory strcime do spojaku
        CFile * newFile = new CFile;
        memcpy(newFile->file.m_FileName,  name, strlen(name) <= FILENAME_LEN_MAX ? strlen(name) : FILENAME_LEN_MAX);
        newFile->file.m_FileName[strlen(name) <= FILENAME_LEN_MAX ? strlen(name) : FILENAME_LEN_MAX] = '\0';
        newFile->startBlock = addr;
        newFile->numBlock = ceil(size/SECTOR_SIZE);
        newFile->file.m_FileSize = size;
        newFile->next = nullptr;
        newFile->before = lastFile;
        if(lastFile != nullptr)
            lastFile->next = newFile;
        if(lastFile == nullptr) {
            ourFiles = newFile;
            iterator = newFile;
        }
        lastFile = newFile;
        numFiles ++;
    }
    CFile * returnFile(const char * fileName, bool & founded){
        char name[29];
        memcpy(name, fileName,strlen(fileName) <= FILENAME_LEN_MAX ? strlen(fileName) : FILENAME_LEN_MAX);
        name[strlen(fileName) <= FILENAME_LEN_MAX ? strlen(fileName) : FILENAME_LEN_MAX] = '\0';
        CFile * nextFile = ourFiles;
        founded = false;
        for(uint32_t i = 0; i < numFiles; i++){
            if (strcmp(nextFile->file.m_FileName, name) == 0){
                founded = true;
                break;
            }
            nextFile = nextFile->next;
        }
        return nextFile;
    }
    uint32_t * readFromFat(uint32_t & index, uint32_t lastSector){
        int32_t * fat = new int32_t [createDev.m_Sectors];
        uint32_t numSector = (createDev.m_Sectors * sizeof (uint32_t)) / SECTOR_SIZE;
        createDev.m_Read(0, fat, numSector);
        int size = 5;
        uint32_t * buffer = (uint32_t *)malloc(size * sizeof(uint32_t));
        int bufferIndex = 0;
        bool write = false;
        if(lastSector == UINT32_MAX) write = true;
        while(true){
            if(fat[index] == myEOF) {
                if (!write)
                    break;
                buffer[bufferIndex] = index;
                bufferIndex ++;
                break;
            }
            if(bufferIndex == size - 1){
                size = size * 2;
                buffer = (uint32_t *)realloc(buffer, size * sizeof(uint32_t));
            }
            if( write ) {
                buffer[bufferIndex] = index;
                bufferIndex++;
            }
            if( index == lastSector) write = true;
            index = fat[index];
        }
        index = bufferIndex;
        delete [] fat;
        return buffer;
    }
    uint32_t * removeFromFat(uint32_t start, uint32_t & size){
        uint32_t numSector = (createDev.m_Sectors * sizeof (uint32_t)) / SECTOR_SIZE;
        int32_t * fat = new int32_t [createDev.m_Sectors];
        createDev.m_Read(0, fat, numSector);
        uint32_t index = start;
        uint32_t bufferSize = 5;
        size = 1;
        uint32_t * buffer = (uint32_t *)malloc(sizeof(uint32_t)*bufferSize);
        buffer[0] = index;
        while(fat[index] != myEOF){
            uint32_t tmp = index;
            index = fat[index];
            if(size >= bufferSize){
                bufferSize = bufferSize * 2;
                buffer = (uint32_t *)realloc(buffer, sizeof(uint32_t)*bufferSize);
            }
            buffer[size] = fat[tmp];
            size ++;
            fat[tmp] = MY_FREE;

        }
        fat[index] = MY_FREE;
        createDev.m_Write(0, fat, numSector);
        delete[] fat;
        return buffer;
    }
    void removeFromBitmap(uint32_t * removed, uint32_t size){
        uint32_t numSector = (createDev.m_Sectors * sizeof (uint32_t)) / SECTOR_SIZE;
        uint32_t * bitmap = new uint32_t [createDev.m_Sectors/32];
        createDev.m_Read(numSector, bitmap, createDev.m_Sectors/(32 * SECTOR_SIZE/4 ) );
        for(uint32_t i = 0; i < size; i++){
            uint row = removed[i] / 32;
            uint32_t num = bitmap[row];
            uint column = removed[i] % 32 + 1;
            num = num - (1 << (32 - column));
            bitmap[row] = num;
        }
        createDev.m_Write(numSector, bitmap, createDev.m_Sectors/(32 * SECTOR_SIZE/4 ) );
        delete [] bitmap;
    }
    void setSizeDir (const char * fileName, uint32_t size){
        CDir addDir[DIR_ENTRIES_MAX];
        //nacteme si adresar souboru
        uint32_t numSector = (createDev.m_Sectors * sizeof (uint32_t)) / SECTOR_SIZE;
        createDev.m_Read(numSector + createDev.m_Sectors/(32 * SECTOR_SIZE/4 ) , addDir, (DIR_ENTRIES_MAX * sizeof (CDir))/SECTOR_SIZE );
        for(uint32_t i = 0; i < DIR_ENTRIES_MAX; i++) {
            if(strcmp(addDir[i].m_name,fileName) == 0){
                addDir[i].m_size = size;
                createDev.m_Write(numSector + createDev.m_Sectors/(32 * SECTOR_SIZE/4 ) , addDir, (DIR_ENTRIES_MAX * sizeof (CDir))/SECTOR_SIZE );
                return;
            }
        }
    }
    void setAddresDir (const char * fileName, uint32_t addr){
        CDir addDir[DIR_ENTRIES_MAX];
        //nacteme si adresar souboru
        uint32_t numSector = (createDev.m_Sectors * sizeof (uint32_t)) / SECTOR_SIZE;
        createDev.m_Read(numSector + createDev.m_Sectors/(32 * SECTOR_SIZE/4 ) , addDir, (DIR_ENTRIES_MAX * sizeof (CDir))/SECTOR_SIZE );
        for(uint32_t i = 0; i < DIR_ENTRIES_MAX; i++) {
            if(strcmp(addDir[i].m_name, fileName) == 0){
                addDir[i].m_addr = addr;
                createDev.m_Write(numSector + createDev.m_Sectors/(32 * SECTOR_SIZE/4 ) , addDir, (DIR_ENTRIES_MAX * sizeof (CDir))/SECTOR_SIZE );
                return;
            }
        }
    }
    void writeToDir(const char * fileName, uint32_t start){
        CDir addDir[DIR_ENTRIES_MAX];
        //nacteme si adresar souboru
        uint32_t numSector = (createDev.m_Sectors * sizeof (uint32_t)) / SECTOR_SIZE;
        createDev.m_Read(numSector + createDev.m_Sectors/(32 * SECTOR_SIZE/4 ) , addDir, (DIR_ENTRIES_MAX * sizeof (CDir))/SECTOR_SIZE );
        for(uint32_t i = 0; i < DIR_ENTRIES_MAX; i++) {
            if(addDir[i].m_addr == UINT32_MAX){
                memcpy(addDir[i].m_name, fileName, strlen(fileName) <= FILENAME_LEN_MAX ? strlen(fileName) : FILENAME_LEN_MAX);
                addDir[i].m_name[strlen(fileName) <= FILENAME_LEN_MAX ? strlen(fileName) : FILENAME_LEN_MAX] ='\0';
                addDir[i].m_addr = start;
                addDir[i].m_size = 0;
                createDev.m_Write(numSector + createDev.m_Sectors/(32 * SECTOR_SIZE/4 ) , addDir, (DIR_ENTRIES_MAX * sizeof (CDir))/SECTOR_SIZE );

                break;
            }
        }
    }
    uint32_t myPow(uint32_t base, uint32_t exponent){
        uint32_t tmp = base;
        if (exponent == 0)
            return 1;
        for (uint32_t i = 1 ; i != exponent ; i++) {
            tmp *= base;
        }
        return tmp;
    }
    void removeFromDir(char * name){
        CDir  addDir[DIR_ENTRIES_MAX];
        //nacteme si adresar souboru
        uint32_t numSector = (createDev.m_Sectors * sizeof (uint32_t)) / SECTOR_SIZE;
        createDev.m_Read(numSector + createDev.m_Sectors/(32 * SECTOR_SIZE/4 ) ,
                         addDir, (DIR_ENTRIES_MAX * sizeof (CDir))/SECTOR_SIZE );
        for (int i = 0 ; i < DIR_ENTRIES_MAX; i++){
            if (strcmp(addDir[i].m_name, name) == 0){
                addDir[i].m_addr = UINT32_MAX;
                addDir[i].m_size = 0;
                for (int j = 0 ; j < FILENAME_LEN_MAX + 1 ; j++ )
                    addDir[i].m_name[j] = '\0';
                createDev.m_Write(numSector + createDev.m_Sectors/(32 * SECTOR_SIZE/4 ) ,
                                  addDir, (DIR_ENTRIES_MAX * sizeof (CDir))/SECTOR_SIZE );
                break;
            }
        }
    }
    void intToChar(uint32_t n , char * mem){
        int i;
        char tmp[33];
        for( i=0; n > 0; i++){
            tmp[i]=char(n%2 + 48);
            n=n/2;
        }
        for (; i != 32 ; i++)
            tmp[i] = '0';
        tmp[32] = '\0';

        // reversing tmp
        int end = strlen(tmp) - 1;
        size_t begin;
        for (begin = 0; begin < strlen(tmp); begin++) {
            mem[begin] = tmp[end];
            end--;
        }

        mem[begin] = '\0';
    }
    int removeFirstZero(uint32_t & number){
        char tmp[33];
        intToChar(number, tmp);
        int index = -1;
        for (int i = 0 ; i < 32 ; i++){
            if (tmp[i] == '0'){
                tmp[i] = '1';
                index = i;
                break;
            }
        }
        if(index == -1) return -1;
        if (index != 31) {
            number = 0;
            for (int i = 0; i < 32; i++) {
                if (tmp[i] == '1')
                    number += myPow(2, 31 - i);
            }
        }
        else
            number = 4294967295;
        return index;
    }
    int findNextIndex(CFile * nextFile){
        for(int i = 0; i < OPEN_FILES_MAX; i++ ){
            if(fileDetec[i] == nullptr){
                fileDetec[i] = nextFile;
                return i;
            }
        }
        return -1;
    }
    int32_t findFreeSector(){
        uint32_t numSector = (createDev.m_Sectors * sizeof (uint32_t)) / SECTOR_SIZE;
        uint32_t * bitmap = new uint32_t [createDev.m_Sectors/32];
        createDev.m_Read(numSector, bitmap, createDev.m_Sectors/(32 * SECTOR_SIZE/4 ) );
        for(unsigned long i = 0; i < createDev.m_Sectors/32; i++ ){
            if (bitmap[i] < UINT32_MAX ){
                int index = removeFirstZero(bitmap[i]);
                createDev.m_Write(numSector, bitmap, createDev.m_Sectors/(32 * SECTOR_SIZE/4 ));
                delete [] bitmap;
                return (i*32)+index;
            }
        }
        delete [] bitmap;
        return -1;
    }
    uint32_t findLastSector(uint32_t start){
        uint32_t numSector = (createDev.m_Sectors * sizeof (uint32_t)) / SECTOR_SIZE;
        int32_t * fat = new int32_t [createDev.m_Sectors];
        createDev.m_Read(0, fat, numSector);
        uint32_t index = start;
        while(fat[index] != myEOF){
            index = fat[index];
        }
        delete [] fat;
        return index;
    }
    void writeToFat(int32_t sector, uint32_t startBlock){
        int32_t * fat = new int32_t [createDev.m_Sectors];
        uint32_t numSector = (createDev.m_Sectors * sizeof (uint32_t)) / SECTOR_SIZE;
        createDev.m_Read(0, fat, numSector);
        int32_t index = startBlock;
        if (startBlock == UINT32_MAX){
            fat[sector] = myEOF;
            createDev.m_Write(0, fat, numSector);
            delete [] fat;
            return;
        }
        while(fat[index] != myEOF){
            index = fat[index];
        }
        fat[index] = sector;
        fat[sector] = myEOF;
        createDev.m_Write(0, fat, numSector);
        delete [] fat;
    }
};


#ifndef __PROGTEST__
#include "./simple_test-mega.inc"
#endif /* __PROGTEST__ */
