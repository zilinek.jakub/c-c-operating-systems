#ifndef __PROGTEST__
#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <cstring>
#include <pthread.h>
#include <semaphore.h>
#include <vector>
#include <iostream>
#include <cmath>
#include "common.h"
#endif /* __PROGTEST__ */
using namespace std;

pthread_mutex_t vectorMutex;
pthread_mutex_t processMutex;

uint8_t RunningProcesses = 1;

pthread_cond_t cond = PTHREAD_COND_INITIALIZER;

class AMD : public CCPU
{
public:
    AMD(uint8_t *memStart, uint32_t pageTableRoot);

    virtual uint32_t GetMemLimit () const;
    virtual bool SetMemLimit ( uint32_t pages );
    virtual bool NewProcess ( void * processArg, void (* entryPoint) ( CCPU *, void * ), bool copyMem );
    void PrintRedPage();
    void PrintOrangePages();
    bool ZeroPage(uint32_t index);
    bool WriteToIndex(uint32_t index, uint32_t value);
    uint32_t ReadFromIndex(uint32_t index);
    ///=================================================================================================================
    /// creates orange page, true success, false failed
    bool CreateOrange();
protected:
    // index of Red page we are writing to
    uint32_t m_RedIndex;

    // counter for free pages on last orange page
    uint32_t m_OrangeLeft;

    // index of Orange page we are writing to
    uint32_t m_OrangeIndex;

    // user alocated space
    uint32_t m_MemLimit;

    // m_memStart casted to uint32t
    uint32_t * m_memStart32;

    //index of red page
    uint32_t m_PageTableRootIdx;
};
// new
class MyVector{
public:
    MyVector() = default;
    MyVector(uint32_t totalPages){
        m_size = totalPages;
        stack = new uint32_t[totalPages];
        uint32_t ind = 0;
        for (int i = int(totalPages) - 1 ; i != -1; i-- ){
            stack[ind] = i;
            ind ++;
        }
        index = totalPages - 1;
    };
    void DELETEMEME() const{
        delete[] stack;
    }

    uint32_t pop_back(){
        pthread_mutex_lock(&vectorMutex);
        uint32_t  toReturn = stack[index];
        if (index != 0)
            index--;
        m_size--;
        pthread_mutex_unlock(&vectorMutex);
        return toReturn;
    }
    void push_back(uint32_t number){
        pthread_mutex_lock(&vectorMutex);
        stack[index + 1] = number;
        index ++;
        m_size ++;
        pthread_mutex_unlock(&vectorMutex);
    }

    uint32_t size () const{
        return m_size;
    }

    uint32_t * stack;
    uint32_t m_size;
    uint32_t index;
};

MyVector freePages;

struct Thread{
    ~Thread(){
        delete cpu;
    }
    void (* m_func) ( CCPU *, void * );
    void * args;
    AMD * cpu;
};

void * Func(void * mem){
    Thread * xyz = (Thread *)mem;
    xyz->m_func(xyz->cpu , xyz->args);
    xyz->cpu->SetMemLimit(0);
    delete xyz;
    pthread_mutex_lock(&processMutex);
    RunningProcesses --;
    pthread_mutex_unlock(&processMutex);
    pthread_cond_signal(&cond);
    return nullptr;
}

AMD::AMD(uint8_t *memStart, uint32_t pageTableRoot) :
        CCPU(memStart, pageTableRoot << 12 | 103) {
    m_memStart32 = (uint32_t *) memStart;
    m_RedIndex = 0;
    m_OrangeIndex = PAGE_DIR_ENTRIES;
    m_MemLimit = 0;
    m_OrangeLeft = 0;
    m_PageTableRootIdx = pageTableRoot;
    ZeroPage(pageTableRoot);
}

uint32_t         AMD::GetMemLimit                   () const{
    return m_MemLimit;
}
bool             AMD::SetMemLimit                   ( uint32_t pages ){
    if (pages == 1027){
        PrintRedPage();
        PrintOrangePages();
    }
    if (m_MemLimit < pages){
        uint32_t tmp = pages - m_MemLimit;
        uint32_t toCreate = m_OrangeLeft > pages - m_MemLimit ? tmp : uint32_t (ceil(double(tmp - m_OrangeLeft) / double(PAGE_DIR_ENTRIES))) + tmp;
        if (toCreate > freePages.size())
            return false;
        for (uint32_t i = 0 ; i < tmp ; i++ ){
            if (m_OrangeLeft == 0){
                CreateOrange();
            }
            uint32_t page = freePages.pop_back();
            uint32_t orange = ReadFromIndex((m_PageTableRootIdx*PAGE_DIR_ENTRIES) + m_RedIndex - 1);
            uint32_t idx = orange * PAGE_DIR_ENTRIES + m_OrangeIndex;
            WriteToIndex( idx , page );
            m_OrangeLeft --;
            m_OrangeIndex ++;
        }
    }
    else if (m_MemLimit > pages){
        uint32_t tmp = m_MemLimit - pages;
        for (uint32_t i = 0 ; i != tmp ; i++){
            if (m_OrangeIndex == 0){
                uint32_t toReturn = int(ReadFromIndex(--m_RedIndex + m_PageTableRootIdx * PAGE_DIR_ENTRIES));
                freePages.push_back(toReturn);
                m_memStart32[m_RedIndex + m_PageTableRootIdx * PAGE_DIR_ENTRIES] = 0;
                m_OrangeIndex = 1024;
                m_OrangeLeft = 0;
            }
            uint32_t idx = ReadFromIndex(m_PageTableRootIdx * PAGE_DIR_ENTRIES + m_RedIndex - 1) * 1024 + m_OrangeIndex - 1;
            m_OrangeLeft++;
            m_OrangeIndex --;
            uint32_t toReturn = ReadFromIndex(idx);
            freePages.push_back( toReturn );
            m_memStart32[idx] = 0;
        }
    }
    if (m_OrangeIndex == 0){
        uint32_t toReturn = uint32_t(ReadFromIndex(--m_RedIndex + m_PageTableRootIdx * PAGE_DIR_ENTRIES));
        freePages.push_back(toReturn);
        m_memStart32[m_RedIndex + m_PageTableRootIdx * PAGE_DIR_ENTRIES] = 0;
        m_OrangeIndex = 1024;
        m_OrangeLeft = 0;
    }
    cout << "===============================================================" << endl;
    if (pages == 1027){
        PrintOrangePages();
    }

    m_MemLimit = pages;
    return true;
}
bool             AMD::NewProcess                    ( void            * processArg,
                                                      void           (* entryPoint) ( CCPU *, void * ),
                                                      bool              copyMem ){
    if (RunningProcesses == 64)
        return false;
    pthread_mutex_lock(&processMutex);
    RunningProcesses ++;
    pthread_mutex_unlock(&processMutex);


    Thread * tmp = new Thread;
    tmp->m_func = entryPoint;
    tmp->args = processArg;
    uint32_t newRedPage = freePages.pop_back();
    tmp->cpu = new AMD(m_MemStart , newRedPage);
    if (copyMem){
        tmp->cpu->SetMemLimit(this->GetMemLimit());
        for ( uint32_t addr = 0; addr < this->GetMemLimit() * CCPU::PAGE_SIZE; addr += 32 ){
            uint32_t jedna;
            this->ReadInt(addr , jedna);
            tmp->cpu->WriteInt(addr, jedna);
        }
    }
    pthread_t thr;
    pthread_create(&thr, NULL , Func, tmp);
    pthread_detach(thr);
    return true;
}
void AMD::PrintRedPage(){
    for (uint32_t i = 0; i < 1024 ; i++){
        if (i % 20 == 0)
            cout << endl;
        cout << ReadFromIndex(m_PageTableRootIdx * 1024 + i) << " ";
    }
    cout << "\n\n\tRED PAGE DONE\n\n";
}
void AMD::PrintOrangePages(){
    for (uint32_t i = 0; i < m_RedIndex ; i++){
        uint32_t idx = ReadFromIndex(m_PageTableRootIdx * PAGE_DIR_ENTRIES + i);
        for (uint32_t j = 0 ; j < PAGE_DIR_ENTRIES ; j++){
            if (j % 20 == 0)
                cout << endl;
            cout << ReadFromIndex((idx * 1024) + j) << " ";
        }
        cout << "\n\n\tNEW ORANGE PAGE\n\n" << endl;
    }
}
bool AMD::ZeroPage(uint32_t index){
    for (uint32_t i = index * PAGE_DIR_ENTRIES; i < index * PAGE_DIR_ENTRIES + PAGE_DIR_ENTRIES  - 1; i++ )
        m_memStart32[i] = 0;
    return true;
}
bool AMD::WriteToIndex(uint32_t index, uint32_t value){
    value = value << 12;
    value = value | 103;
    m_memStart32[index] = value;
    return true;
}
uint32_t AMD::ReadFromIndex(uint32_t index){
    uint32_t value = m_memStart32[index];
    value = value >> 12;
    return value;
}
bool AMD::CreateOrange(){
    uint32_t value = freePages.pop_back();
    uint32_t index = m_PageTableRootIdx * PAGE_DIR_ENTRIES + m_RedIndex;
    m_RedIndex ++;
    m_OrangeIndex = 0;
    m_OrangeLeft = 1024;
    WriteToIndex(index, value);
    ZeroPage(value);
    return true;
}
void               MemMgr                                  ( void            * mem,
                                                             uint32_t          totalPages,
                                                             void            * processArg,
                                                             void           (* mainProcess) ( CCPU *, void * ))
{
    freePages = MyVector(totalPages) ;
    uint32_t newRed = freePages.pop_back();

    AMD myCPU(( uint8_t* )mem , newRed);
    mainProcess(&myCPU , processArg);
    pthread_mutex_lock(&processMutex);
    while(RunningProcesses != 1){
        pthread_cond_wait(&cond , &processMutex );
    }
    pthread_mutex_unlock(&processMutex);

    freePages.DELETEMEME();
    return;
}
