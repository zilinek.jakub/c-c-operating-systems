#ifndef __PROGTEST__
#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <cstring>
#include <pthread.h>
#include <semaphore.h>
#include <iostream>
#include <mutex>
#include "common.h"
#include <vector>
#include <cmath>

#endif /* __PROGTEST__ */
class Processor : public CCPU
{
public:
    explicit Processor(uint8_t * memStart);
    virtual uint32_t         GetMemLimit                   ( void ) const;
    virtual bool             SetMemLimit                   ( uint32_t pages );
    virtual bool             NewProcess                    ( void            * processArg,
                                                             void           (* entryPoint) ( CCPU *, void * ),
                                                             bool              copyMem );
protected:
    uint32_t m_memLimit;
    uint32_t m_pageTableRootNum;
    uint32_t m_freeOrange;
    uint32_t countNeedy(uint32_t page);
    bool writeTo(uint32_t num);
    void removePage();
    /*
     if copy-on-write is implemented:

     virtual bool             pageFaultHandler              ( uint32_t          address, bool              write );
     */
};
class Stack{
private:
    unsigned int m_start;
    unsigned int m_end;
    unsigned int m_length;
    unsigned int * m_stack;
    unsigned int m_free;
    void add(unsigned int & num){
        num ++;
        if(num >= m_length){
            num = 0;
        }
    }
public:
    Stack(){
        std::cout << "Ahoj ja jsem constructor" <<std::endl;
    }
    void Init(unsigned int length){
        m_stack = (unsigned int * )malloc(length * sizeof(unsigned int));
        m_start = 0;
        m_end = 0;
        m_length = length;
        m_free = length;
    }
    bool push(unsigned int page){
        if(m_free == 0) return false;
        m_stack[m_end] = page;
        add(m_end);
        m_free ++;
        return true;
    }
    bool pop(unsigned int & page){
        if(m_free == m_length) return false;
        page = m_stack[m_start];
        add(m_start);
        m_free --;
        return true;
    }
    void Fill(){
        for(unsigned int i = 0; i < m_length; i ++){
            push(i);
        }
    }
    unsigned int getFree(){
        return m_free;
    }
    void DeleteMemory(){
        free(m_stack);
    }
    ~Stack(){
        std::cout << "Ahoj ja jsem destructor" <<std::endl;
        //free(m_stack);
    }
};
class ProcessInfo{
public:
    explicit ProcessInfo(){
        runningProcess = 1;
    }
    void Init(uint32_t numberPages){
        freePages.Init(numberPages);
        freePages.Fill();
    }

    uint32_t runningProcess;
    Stack freePages;
};

pthread_mutex_t pages_mutex;
pthread_mutex_t process_mutex;
pthread_cond_t myCond = PTHREAD_COND_INITIALIZER;
static ProcessInfo INFO;
struct ThreadParam{
    ThreadParam() = default;
    ~ThreadParam(){
        printf("Ahoj, mazu ti pamet kterou nechces mazat. Spise pointer.\n\n\n");
    }
    void (* entryPoint) ( CCPU *, void * );
    uint8_t  * memStart;
    void * processArg;
};
void * ThreadFunc( void * z){
    ThreadParam * x = (ThreadParam *) z;
    Processor paramCPU(x->memStart);
    x->entryPoint(&paramCPU , x->processArg);
    pthread_mutex_lock(&process_mutex);
    INFO.runningProcess --;
    pthread_cond_signal(&myCond); //todo ma byt zamkly mutex ?
    pthread_mutex_unlock(&process_mutex);
    return nullptr;
}
#define FORMAT 103


Processor::Processor(uint8_t * memStart) : CCPU(memStart, 0)
{
    uint32_t address;
    //najit novou stranku
    INFO.freePages.pop(address);
    //vyplnit stranku 0
    for(uint32_t i = 0; i < CCPU::PAGE_DIR_ENTRIES; i++){ //todo add  i+4 ??
        uint32_t * ref = (uint32_t *)(m_MemStart + i*4 + (address * CCPU::PAGE_SIZE));
        *ref = 0;
    }
    //nastavit tableRoot na nasi vyplnenou
    m_PageTableRoot = address << 12 | FORMAT;
    m_pageTableRootNum = address;
    m_freeOrange = 0;
    m_memLimit = 0;
}
uint32_t Processor::GetMemLimit ( void ) const{
    return m_memLimit;
}

uint32_t  Processor::countNeedy(uint32_t page){
    uint32_t tmp = page - m_freeOrange;
    return ceil(tmp / PAGE_DIR_ENTRIES) + page;
}
bool Processor::writeTo(uint32_t num){
    unsigned int rootIndex = (m_memLimit / PAGE_DIR_ENTRIES);
    unsigned int tableIndex = (m_memLimit % PAGE_DIR_ENTRIES);
    uint32_t page;
    if(tableIndex == 0){
        if (INFO.freePages.getFree() < 1) {
            return false;
        }
        if (!INFO.freePages.pop(page)) {
            printf("neco je hodne spatne \n");
            return false;
        }
        uint32_t * ref = (uint32_t *) (m_MemStart + rootIndex*4 + (m_pageTableRootNum * CCPU::PAGE_SIZE));
        *ref = page << 12 | FORMAT;

        for(uint32_t i = 0; i < CCPU::PAGE_DIR_ENTRIES; i += 1){ //todo i + 4??
            uint32_t * reftmp = (uint32_t *)(m_MemStart + i*4 + (page * CCPU::PAGE_SIZE));
            *reftmp = 0;
        }
    }
    uint32_t * ref = (uint32_t *)(m_MemStart + rootIndex*4 + (m_pageTableRootNum * CCPU::PAGE_SIZE));
    page =  (*ref & ADDR_MASK) >> 12; /// 0..page....111 -> page
    ref =  (uint32_t *)(m_MemStart + (page * CCPU::PAGE_SIZE) + tableIndex*4);
    *ref = num;
    page = (num & ADDR_MASK) >> 12;
    //todo ma tu byt ten for?
    for(uint32_t i = 0; i < CCPU::PAGE_DIR_ENTRIES; i++){ //todo i + 4??
        uint32_t * reftmp = (uint32_t *)(m_MemStart + i*4 + (page * CCPU::PAGE_SIZE));
        *reftmp = 0;
    }
    return true;
}
void Processor::removePage() {
    unsigned int rootIndex = (m_memLimit / PAGE_DIR_ENTRIES) ;
    unsigned int tableIndex = (m_memLimit % PAGE_DIR_ENTRIES) ;
    uint32_t num;
    unsigned int pushPage;
    // tableIndex = 0 -> zaznam je v predchozi tabulce
    // tableIndex = 1 -> zrusime zaznam i tabulku
    // tableIndex != 1 && != 0  -> zrusime zaznam
    if(tableIndex == 0 ){
        uint32_t * ref = (uint32_t * ) (m_MemStart + rootIndex*4 - 4 + (m_pageTableRootNum * CCPU::PAGE_SIZE));
        uint32_t page = (*ref & ADDR_MASK) >> 12; /// 0.page..111 ->page
        ref = (uint32_t * )(m_MemStart + (page * CCPU::PAGE_SIZE) + CCPU::PAGE_DIR_ENTRIES*4 - 4 );
        num = *ref;
        *ref = 0;
    }
    else if(tableIndex == 1){
        uint32_t * ref = (uint32_t * ) (m_MemStart + rootIndex*4  + (m_pageTableRootNum * CCPU::PAGE_SIZE));
        uint32_t page = (*ref & ADDR_MASK) >> 12;
        *ref = 0;
        ref = (uint32_t * )(m_MemStart + (page * CCPU::PAGE_SIZE) + tableIndex*4 - 4 );
        num = *ref;
        *ref = 0;
        pthread_mutex_lock(&pages_mutex);
        INFO.freePages.push(page);
        pthread_mutex_unlock(&pages_mutex);
    }
    else {
        uint32_t * ref = (uint32_t * ) (m_MemStart + rootIndex*4  + (m_pageTableRootNum * CCPU::PAGE_SIZE));
        uint32_t page = (*ref & ADDR_MASK) >> 12; /// 0.page..111 ->page
        ref = (uint32_t * )(m_MemStart + (page * CCPU::PAGE_SIZE) + tableIndex*4 - 4 );
        num = *ref;
        *ref = 0;
    }
    pushPage = (num & ADDR_MASK) >> 12;
    pthread_mutex_lock(&pages_mutex);
    INFO.freePages.push(pushPage);
    pthread_mutex_unlock(&pages_mutex);

    //todo ma tu byt ten for?
    for(uint32_t i = 0; i < CCPU::PAGE_DIR_ENTRIES; i++){ //todo i + 4??
        uint32_t * reftmp = (uint32_t *)(m_MemStart + i*4 + (pushPage * CCPU::PAGE_SIZE));
        *reftmp = 0;
    }

}
bool Processor::SetMemLimit ( uint32_t pages ){
    if(pages > m_memLimit) {
        uint32_t tmpMem = pages - m_memLimit;
        pthread_mutex_lock(&pages_mutex);
        if (INFO.freePages.getFree() < countNeedy(tmpMem)) {
            pthread_mutex_unlock(&pages_mutex);
            printf("nejsou stranky \n");
            return false;
        }
        std::vector <int> tmpVec;
        for (uint32_t i = 0; i < tmpMem; i++) {
            unsigned int page;
            if (!INFO.freePages.pop(page)) {
                pthread_mutex_unlock(&pages_mutex);
                printf("neco je hodne spatne \n");
                return false;
            }
            tmpVec.push_back(page);
            uint32_t tmp = page << 12 | FORMAT; ///// 000...000101000000000111
            if(!writeTo(tmp)){
                pthread_mutex_unlock(&pages_mutex);
                printf("nezapsali jsme  \n");
                return false;
            }
            m_memLimit ++;
        }
        printf("\n%d -> ", pthread_self()); // gettid()
        for (auto & it : tmpVec)
            printf("%d | ", it);
        printf("\n\n");
        pthread_mutex_unlock(&pages_mutex);
    }
    else if(pages < m_memLimit){
        uint32_t tmpMem = m_memLimit - pages;
        for(uint32_t i = 0; i < tmpMem; i++){
            removePage();
            m_memLimit --;
        }
    }
    return true;
}

ThreadParam param;

bool Processor::NewProcess ( void * processArg, void (* entryPoint) ( CCPU *, void * ), bool copyMem ){
    if(copyMem){
        //todo
        return false;
    }
    pthread_mutex_lock(&process_mutex);
    if(INFO.runningProcess >= PROCESS_MAX){
        pthread_mutex_unlock(&process_mutex);
        return false;
    }
    pthread_mutex_unlock(&process_mutex);

    pthread_mutex_lock(&pages_mutex);
    if(INFO.freePages.getFree() < 1){
        pthread_mutex_unlock(&pages_mutex);
        return false;
    }
    pthread_mutex_unlock(&pages_mutex);
    param.entryPoint = entryPoint;
    param.processArg = processArg;
    param.memStart = m_MemStart;

    pthread_t thread;
//    pthread_attr_t   attr;
//    pthread_attr_init ( &attr );
    pthread_mutex_lock(&process_mutex);
    INFO.runningProcess ++;
    pthread_mutex_unlock(&process_mutex);
    pthread_create(&thread, nullptr, ThreadFunc, &param);
//    pthread_join(thread, NULL);
    pthread_detach(thread);
    return true;
}

void MemMgr ( void * mem, uint32_t totalPages, void * processArg, void (* mainProcess)( CCPU *, void * ))
{

    INFO.Init(totalPages);
    pthread_mutex_lock(&pages_mutex);
    Processor myCPU((uint8_t*)mem);
    pthread_mutex_unlock(&pages_mutex);


    mainProcess(& myCPU , processArg);
    pthread_mutex_lock(&process_mutex); // todo ma to byt zamkle ?
    while(INFO.runningProcess != 1){
        pthread_cond_wait(&myCond, &process_mutex);
    }
    pthread_mutex_unlock(&process_mutex);
    INFO.freePages.DeleteMemory();

}
