#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <cstring>
#include <cstdarg>
#include <pthread.h>
#include <semaphore.h>
#include <iostream>
#include "common.h"
#include "test_op.h"
using namespace std;

static void        seqTest1                                ( CCPU            * cpu,
                                                             void            * arg )
{
  for ( uint32_t i = 0; i <= 2000; i ++ )
    checkResize ( cpu, i );

  checkResize ( cpu, 5000 );

  for ( int i = 2000; i >= 0; i -- )
    checkResize ( cpu, i );
}

static void        seqTest2                                ( CCPU            * cpu,
                                                             void            * arg )
{
    checkResize ( cpu, 1026 );
    rwiTest ( cpu, 0, 1026 );
    checkResize ( cpu, 1027 );
    rwiTest ( cpu, 0, 1027 );
}

int                main                                    ( void )
{
    const int PAGES = 10 * 1024 ;
    uint8_t *mem = new uint8_t[PAGES * CCPU::PAGE_SIZE + CCPU::PAGE_SIZE];
    uint8_t *memAligned = (uint8_t *) ((((uintptr_t) mem) + CCPU::PAGE_SIZE - 1) & ~(uintptr_t) ~CCPU::ADDR_MASK);
    testStart();
    MemMgr(memAligned, PAGES, NULL, seqTest1);
    testEnd("test #1");
    testStart();
    MemMgr(memAligned, PAGES, NULL, seqTest2);
    testEnd("test #2");
    delete[] mem;
  return 0;
}
